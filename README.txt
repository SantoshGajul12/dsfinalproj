//***********************************************************************//
	DOCUMENT AUTHORING SYSTEM & CLOUD
		developed by
	Naser Sharifi <msharifi@luc.edu>
	Santosh Gajul <sgajul@luc.edu>
	Computer Science Department
	Loyola University Chicago
//***********************************************************************//


->Important facts:
1) This application is currently developed with JSP, MySQL and Apache Tomcat
2) Video Streaming is done and distributed to several users
3) Document Authoring concurrently can be implemented using websocket, but challenging.

-> Database:

1) Create a database schema named as "mobile".
2) Copy the script in MySQL workbench or run the script from mysql command line to generate the tables and insert data into tables.
3) Make sure you change the password of your system's  MySQL root user password in src\databaseconnection\databasecon.java file.

-> Apache Tomcat:

1) Just download the Apache Tomcat 8 from "https://tomcat.apache.org/download-80.cgi" and place the cloned directory "dsfinalproj" in webapps folder of Apache Software 2) Foundation available in C Drive
3) Run the Tomcat8 application from the bin folder of the Apache Software Foundation available in C Drive. This will show you that tomcat server runs on 8080 port
4) Go to web browser and run the URL "localhost:8080/dsfinalproj/