<%@ page import="java.sql.*,databaseconnection.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Document Authoring & Cloud</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="datetimepicker.js">

//Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
//Script featured on JavaScript Kit (http://www.javascriptkit.com)
//For this script, visit http://www.javascriptkit.com 

</script>
<script type="text/javascript">


function validation()

{
var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
var m=emailfilter.test(document.form.value);
if(m==false)
{
alert("Please enter a valid Email Id");
document.form.focus();
return false;
}
var v=document.form.out.value;
if(v=="")
{
alert("Enter Your Password");
document.form.out.focus();
return false;
}
}



</script>
<script language="javascript" type="text/javascript">


function valid()
{
var name=document.s.unn.value;
var nameMatch=/^[a-z\A-Z]+$/;
if(!(name.match(nameMatch)))
{
alert("please enter valid name");
document.s.unn.focus();
return false;
}
var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
var m=emailfilter.test(document.s.eidd.value);
if(m==false)
{
alert("Please enter a valid Email Id");
document.s.eidd.focus();
return false;
}
var b=document.s.passs.value;
if(b=="")
{
alert("Enter Password");
document.s.passs.focus();
return false;
}
var k = document.s.mobb.value;
if(k=="")
{
alert("Enter mobile number");
document.s.mobb.focus();
return false;
}
if(isNaN(k))
{
alert("Enter mobile number in numbers");
document.s.mobb.focus();
return false;
}
if(k.charAt(0)!=9)
{
alert("Enter the correct mobile no");
document.s.mobb.focus();
return false;
}
if(k.length!=10)
{
alert("Enter 10 digits");
document.s.mobb.focus();
return false;
}
if(document.s.log.selectedIndex==0)
{
alert("Select Gender");
return false;
}

var aaa=document.s.file.value;
if(aaa=="")
{
alert("Chosse your photo");
document.s.file.focus();
return false;
}
var z=document.s.dob.value;
	if(z=="")
	{
	alert ("pick date");
	document.s.dob.focus();
	return false;
	}

}



</script>
</head>
<body>
<%session.invalidate();%>
<div id="wrapper"> 
  <h1><img src="images/logo.jpg" width="554" height="47" alt="Travel Agency" /></h1>
  <div id="booking"> 
    <h2>Welcome To Document Authoring & Cloud</h2>
    <form action="user.jsp" method="get" name="form" onsubmit="return validation()">
      <div class="jtype"> </div>
      <!-- end .jtype -->
      <table summary="" cellspacing="0" cellpadding="0" border="0">
        <tr> 
          <th>Email</th>
          <td><input name="in" type="text" class="text" /> </td>
        </tr>
        <tr> 
          <th>Password</th>
          <td><input name="out" type="password" class="text" /> <input type="submit" value="submit" class="submit" /></td>
        </tr>
      </table>
    </form>
  </div>
  <!-- end booking -->
  <div id="nav"> 
    <ul>
      <li><a href="info.jsp">Info</a></li>
      <li><a href="friends.jsp">Friends</a></li>
      <li><a href="chat.jsp">Chat</a></li>
      <li><a href="sunscriber.jsp">Subscribe</a></li>
      <li><a href="image.jsp">Sign Up</a></li>
    </ul>
  </div>
  <!-- end nav -->
  <h2  id="packagesheader"><img src="images/title_our_packages.gif" width="352" height="23" alt="our packages" /></h2>
  <div id="packages"> 
    <h3 class="silveren">Sign Up</h3>
    <form name="s" action="newuser.jsp" method="get" onSubmit="return valid()">
      <table>
        <tr> 
          <td width="73"><font face="Times New Roman"  size="+1" color="#666666"><strong>Name</strong></font></td>
          <td width="145">&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="unn" placeholder="Enter your name?"></td>
        </tr>
        <tr> 
          <td ><font face="Times New Roman"  size="+1" color="#666666"><strong>Your 
            Email</strong></font></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="eidd" placeholder="Enter your Email id?"></td>
        </tr>
        <tr> 
          <td><font face="Times New Roman"  size="+1" color="#666666"><strong>Password</strong></font></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp; <input type="password" name="passs" placeholder="Enter your password?"></td>
        </tr>
        <tr> 
          <td ><font face="Times New Roman"  size="+1" color="#666666"><strong>Mobile</strong></font></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="mobb" placeholder="Enter your mobile numbers?"></td>
        </tr>
        <tr> 
          <td ><font face="Times New Roman"  size="+1" color="#666666"><strong>I 
            Am</strong></font></td>
          <td><select name="log">
              <option value="0">select sex:</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select></td>
        </tr>
        <tr> 
          <td ><font face="Times New Roman"  size="+1" color="#666666"><strong>Birthday</strong></font></td>
          <td><input id="demo1" type="text" size="15" name="dob" placeholder="choose your birthday?"> 
            <a href="javascript:NewCal('demo1','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a> 
          </td>
        </tr>
        <tr> 
          <td></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="s" value="submit" class="button" > 
            &nbsp;&nbsp;&nbsp; <input type="reset" name="r" value="clear" class="button"></td>
        </tr>
      </table>
    </form></p>
    <p class="readmore"></p>
    <div id="special"> </div>
    <!-- end special -->
  </div>
  <!-- end packages -->
  <div id="main"> <img src="images/images.jpg" width="447px" height="298px"> 
    <h2><img src="images/title_hottest_locations.gif" width="447" height="24" alt="hottest locations" /></h2>
    <div class="inner"> 
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end main -->
  <div id="footer"> </div>
  <!-- end footer -->
</div>
<!-- end wrapper -->

<div align=center></div></body>
</html>
